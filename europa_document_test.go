package europaDocuments

import (
	"fmt"
	"reflect"
	"strings"
	"testing"

	documents "bitbucket.org/matchmove/risk-document"
)

const (
	ProcessorSource = "documents/source/"
)

func TestNewEuropaDocument(t *testing.T) {
	files, err := documents.GetLatestFiles(ProcessorSource)
	if err != nil {
		t.Errorf("Exception occured in files.GetLatest: %s", err)
	}

	for _, file := range files {
		if strings.Contains(strings.ToLower(file.Filename), SourceEuropaEU) {

			document := NewEuropaDocument(file)

			if "main.EuropaDocument" != fmt.Sprint(reflect.TypeOf(document)) {
				t.Errorf("NewEuropaDocument instance expecting:%s, got:%s", "main.EuropaDocument", string(fmt.Sprint(reflect.TypeOf(document))))
			}

			document.Parse()
			document.Format()
		}
	}
}

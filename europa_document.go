package europaDocuments

import (
	"encoding/xml"
	"fmt"
	"strings"

	documents "bitbucket.org/matchmove/risk-document"
	"bitbucket.org/matchmove/risk-interests"
)

const (
	//SourceEuropaEU  ...
	SourceEuropaEU = "ec.europa.eu"
)

// EuropaDocument represent the file to be process
type EuropaDocument struct {
	documents.DocumentObject
	Reference string
	Interests interests.Interests
}

// EuropaDocumentQuery xml struct
type EuropaDocumentQuery struct {
	XMLName  xml.Name               `xml:"WHOLE"`
	Entities []EuropaDocumentEntity `xml:"ENTITY"`
}

// EuropaDocumentCitizen xml struct
type EuropaDocumentCitizen struct {
	XMLName xml.Name `xml:"CITIZEN"`
	Country string   `xml:"COUNTRY"`
}

// EuropaDocumentBirth xml struct
type EuropaDocumentBirth struct {
	XMLName  xml.Name `xml:"BIRTH"`
	Birthday string   `xml:"DATE"`
	Place    string   `xml:"PLACE"`
	Country  string   `xml:"COUNTRY"`
}

// EuropaDocumentPassport xml struct
type EuropaDocumentPassport struct {
	XMLName xml.Name `xml:"PASSPORT"`
	ID      string   `xml:"Id,attr"`
	Number  string   `xml:"NUMBER"`
	Country string   `xml:"COUNTRY"`
}

// EuropaDocumentEntity xml struct
type EuropaDocumentEntity struct {
	XMLName   xml.Name                      `xml:"ENTITY"`
	ID        string                        `xml:"Entity_id,attr"`
	Type      string                        `xml:"Type,attr"`
	Names     EuropaDocumentEntityNames     `xml:"NAME"`
	Addresses []EuropaDocumentEntityAddress `xml:"ADDRESS"`
	Birth     []EuropaDocumentBirth         `xml:"BIRTH"`
	Passport  EuropaDocumentPassport        `xml:"PASSPORT"`
	Citizen   EuropaDocumentCitizen         `xml:"CITIZEN"`
}

// EuropaDocumentEntityName xml struct
type EuropaDocumentEntityName struct {
	XMLName  xml.Name `xml:"NAME"`
	ID       string   `xml:"Id,attr"`
	Last     string   `xml:"LASTNAME"`
	First    string   `xml:"FIRSTNAME"`
	Middle   string   `xml:"MIDDLENAME"`
	Whole    string   `xml:"WHOLENAME"`
	Gender   string   `xml:"GENDER"`
	Title    string   `xml:"TITLE"`
	Function string   `xml:"FUNCTION"`
	Language string   `xml:"LANGUAGE"`
}

// EuropaDocumentEntityNames xml struct
type EuropaDocumentEntityNames []EuropaDocumentEntityName

// EuropaDocumentEntityAddress xml struct
type EuropaDocumentEntityAddress struct {
	XMLName xml.Name `xml:"ADDRESS"`
	ID      string   `xml:"Id,attr"`
	Number  string   `xml:"NUMBER"`
	Street  string   `xml:"STREET"`
	ZipCode string   `xml:"ZIPCODE"`
	City    string   `xml:"CITY"`
	Country string   `xml:"COUNTRY"`
	Other   string   `xml:"OTHER"`
}

// NewEuropaDocument initilized EuropaDocument
func NewEuropaDocument(f documents.File) EuropaDocument {
	f.ReadFile()

	document := EuropaDocument{}
	document.Body = f.Contents
	document.Reference = f.Path + f.Filename
	return document
}

func getCompleteAddresses(a []EuropaDocumentEntityAddress) []string {
	var values []string

	for _, item := range a {
		values = append(
			values,
			fmt.Sprintf("address id %s: %s %s %s %s %s", item.ID, item.Number, item.Street, item.City, item.ZipCode, item.Country),
		)
	}

	return (values)
}

func getArrAddresses(key string, a []EuropaDocumentEntityAddress) []string {
	var values []string
	for _, item := range a {
		switch key {
		case "zipcode":
			values = append(values, item.ZipCode)
			break
		case "country":
			values = append(values, item.Country)
			break
		}
	}
	return (values)

}

func getArrBirth(key string, n []EuropaDocumentBirth) []string {
	var values []string
	for _, item := range n {
		switch key {
		case "place":
			values = append(values, item.Place)
			break
		case "country":
			values = append(values, item.Country)
			break
		case "date":
			values = append(values, item.Birthday)
			break
		}
	}
	return (values)
}

func getArrValues(key string, n EuropaDocumentEntityNames) []string {
	var values []string
	for _, item := range n {
		switch key {
		case "name":
			values = append(values, item.Whole)
			break
		case "gender":
			values = append(values, item.Gender)
			break
		case "title":
			values = append(values, item.Title)
			break
		case "function":
			values = append(values, item.Function)
			break
		case "language":
			values = append(values, item.Language)
			break
		}
	}
	return (values)
}

// Parse breaks d.Body into arrays
func (d *EuropaDocument) Parse() {
	var query EuropaDocumentQuery
	decoder := xml.NewDecoder(strings.NewReader(d.Body))
	decoder.Strict = false
	decoder.Decode(&query)

	var interestType string
	var countries []string

	for _, entity := range query.Entities {

		countries = getArrAddresses("country", entity.Addresses)
		countries = append(countries, getArrBirth("country", entity.Birth)...)
		countries = append(countries, entity.Passport.Country)
		countries = append(countries, entity.Citizen.Country)

		if entity.Type == "P" {
			interestType = interests.InterestTypeIndividual
		} else {
			interestType = interests.InterestTypeEntities
		}

		names := getArrValues("name", entity.Names)

		var documents []string
		if entity.Passport.Number != "" {
			documents = append(documents, "Passport:"+entity.Passport.Number)
		}

		d.Interests = append(d.Interests, interests.Interest{
			ID:               entity.ID,
			Source:           SourceEuropaEU,
			Reference:        d.Reference,
			Name:             names[0],
			AliasGoodQuality: names,
			Type:             interestType,
			Gender:           getArrValues("gender", entity.Names),
			Title:            strings.Join(getArrValues("title", entity.Names), ","),
			Designation:      strings.Join(getArrValues("function", entity.Names), ","),
			Address:          getCompleteAddresses(entity.Addresses),
			POBTown:          strings.Join(getArrBirth("place", entity.Birth), ","),
			POBCountry:       strings.Join(getArrBirth("country", entity.Birth), ","),
			DOB:              getArrBirth("date", entity.Birth),
			Documents:        documents,
			Country:          (countries),
		})

	}
}

// Format parse the array in d.Entities and d.Individuals
// and assigns them to an array of Interest
func (d *EuropaDocument) Format() interests.Interests {
	return d.Interests
}
